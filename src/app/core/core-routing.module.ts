import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoreLayoutComponent } from './layout/core-layout/core-layout.component';


const routes: Routes = [
  {
    path: '', component: CoreLayoutComponent,
    children: [
      { path: '', loadChildren: () => import('./../intranet/intranet.module').then(m => m.IntranetModule) },
      { path: 'warehouse', loadChildren: () => import('./../warehouse/warehouse.module').then(m => m.WarehouseModule) }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
