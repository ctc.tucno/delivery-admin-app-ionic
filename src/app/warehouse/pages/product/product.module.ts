import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductPageRoutingModule } from './product-routing.module';

import { ProductPage } from './product.page';
import { ListProductComponent } from '../../components/list-product/list-product.component';
import { AddProductComponent } from '../../components/add-product/add-product.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductPageRoutingModule
  ],
  declarations: [
    ProductPage,
    ListProductComponent,
    AddProductComponent
  ]
})
export class ProductPageModule {}
