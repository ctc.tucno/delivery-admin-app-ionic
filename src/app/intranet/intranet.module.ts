import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntranetRoutingModule } from './intranet-routing.module';
import { IntranetLayoutComponent } from './layout/intranet-layout/intranet-layout.component';


@NgModule({
  declarations: [
    IntranetLayoutComponent
  ],
  imports: [
    CommonModule,
    IntranetRoutingModule
  ]
})
export class IntranetModule { }
