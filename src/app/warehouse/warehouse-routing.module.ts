import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WarehouseLayoutComponent } from './layout/warehouse-layout/warehouse-layout.component';


const routes: Routes = [
  {
    path: '', component: WarehouseLayoutComponent,
    children: [
      {
        path: 'home',
        loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
      },
      {
        path: 'product',
        loadChildren: () => import('./pages/product/product.module').then( m => m.ProductPageModule)
      }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WarehouseRoutingModule { }
