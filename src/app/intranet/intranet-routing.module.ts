import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntranetLayoutComponent } from './layout/intranet-layout/intranet-layout.component';


const routes: Routes = [
  { path:'',component:IntranetLayoutComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntranetRoutingModule { }
