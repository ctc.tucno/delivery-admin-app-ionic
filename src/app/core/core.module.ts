import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { CoreLayoutComponent } from './layout/core-layout/core-layout.component';
import { IonicModule } from '@ionic/angular';


@NgModule({
  declarations: [
    CoreLayoutComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    CoreRoutingModule
  ]
})
export class CoreModule { }
